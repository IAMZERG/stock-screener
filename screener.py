import alpaca_trade_api as tradeapi
import datetime
import os
#from pathlib import Path
from polygon import RESTClient
import time
import pandas as pd
import requests
import json

import numpy as np


def grabStocks():
    URL = "https://api.polygon.io/v2/snapshot/locale/us/markets/stocks/tickers"


    time.sleep(1)
    res = requests.get(URL + "?apiKey=" + os.environ.get('APCA_API_KEY_ID'))
    res_json = json.loads(res.text)
    trades = res_json

    return trades['tickers']

def filterData(tickers):
    filtered = []
    for i in tickers:
        if i['prevDay']['v'] >= 1000000:
            filtered.append(i)

    return filtered
        
