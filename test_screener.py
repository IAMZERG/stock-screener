import alpaca_trade_api as tradeapi
import datetime
import os
#from pathlib import Path
from polygon import RESTClient
import time
import pandas as pd
import requests
import json
import sys

import numpy as np

import unittest
import screener

sys.path.append("~/projects/stock_screener")

class grabStocksTestCases(unittest.TestCase):
    def test_grabStocks_returns_something(self):
        results = screener.grabStocks()
        self.assertTrue(len(results) > 0)
    def test_ticker_has_expected_value(self):
        results = screener.grabStocks()
        self.assertTrue(type(results[0]['ticker']) == type("string"))
class filterDataTestCases(unittest.TestCase):
    def setUp(self):
        self.stocks = screener.grabStocks()
    def test_filterData_returns_fewer_results(self):
        results = screener.filterData(self.stocks)
        #results should be list of stocks, and there should be less than those from stocks
        self.assertTrue(len(results) < len(self.stocks))


if __name__ == '__main__':
    unittest.main()

        
